# Ms-Hermes

Given a technical limitation to implement Arena (Bull-Arena) with Koa, it was decided to implement an API that provides the service by express.

# heroku

Name Heroku: ms-hermes

host-dev: https://ms-hermes.herokuapp.com

**Configure environment variables**

Example:
> heroku config:set REDIS_HOST=redis-13917.c44.us-east-1-2.ec2.cloud.redislabs.com

Know Your Foe: Would You Like to Know **More**?

* Heroku
  * [Deploy your application to Heroku](https://devcenter.heroku.com/articles/deploying-nodejs#specifying-a-start-script)
  * Environment variables - [Cache behavior](https://devcenter.heroku.com/articles/nodejs-support#cache-behavior)