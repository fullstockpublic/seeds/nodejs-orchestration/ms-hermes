import {ServerLoader, ServerSettings, GlobalAcceptMimesMiddleware} from "@tsed/common";
import * as Arena from 'bull-arena';
import * as dotenv from "dotenv";
import * as Redis from "ioredis";

import Path = require("path");
import Queue = require("bull");
import { queues, Themisto, Ganymede } from "./amqp/Queue";

@ServerSettings({
    rootDir: Path.resolve(__dirname), // optional. By default it's equal to process.cwd()
    acceptMimes: ["application/json"]
})
export class Server extends ServerLoader {

    /**
     * This method let you configure the express middleware required by your application to works.
     * @returns {Server}
     */
    public $onMountingMiddlewares(): void|Promise<any> {
    
        if (process.env.NODE_ENV !== "production") {
            // Used only in development to load environment variables from local file.
            dotenv.config();
            
        }

        this.settings.port = process.env.PORT || 8080;

        const cookieParser = require('cookie-parser'),
            bodyParser = require('body-parser'),
            compress = require('compression'),
            methodOverride = require('method-override');

        this
            .use(GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
                extended: true
            }))
            .use(new Arena({
                queues: [
                    /* {
                        // Name of the bull queue, this name must match up exactly with what you've defined in bull.
                        name: 'Hermes',
                    
                        // Hostname or queue prefix, you can put whatever you want.
                        hostId: "redis-13917.c44.us-east-1-2.ec2.cloud.redislabs.com",
                    
                        // Redis auth.
                        redis: {
                            port: process.env.REDIS_PORT || 6379, 
                            host: process.env.REDIS_HOST || '127.0.0.1', 
                            password: process.env.REDIS_PASS || 'localPass'
                        },
                    }    */
                    queues[Themisto],
                    queues[Ganymede]
                ]
              }));

        return null;
    }

    public $onReady(){
        console.log('Server started...');
    }
   
    public $onServerInitError(err){
        console.error(err);
    }
}

new Server().start();